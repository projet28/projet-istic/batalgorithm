import java.util.Random;
import java.util.concurrent.Semaphore;

public class BatAlgo extends binMeta{

	private Random R;
	
	private int nbThread;
	
	private int nbBats;
	
	//semaphore pour g�rer la concurence lors des acc�s aux variables volatile
	private Semaphore sem;
	
	//solution et valeur volatile pour communiquer entre thread
	private volatile Data best;
	private volatile double bestValue;
	
	
	private class Bats implements Runnable{
		private int nbBat;
		
		private Data[] bestLocal;//meilleur valeur courante par bat
		
		private Data[] x;//position
		private int[] v;//vitesse
		private double[] f;//fr�quence
		private double[] r;//taux
		private double[] initialR;//taux de d�part
		private double[] a;//bruit
		
		private int id;//borne
		
		public Bats(int nbBat, Data D, int intDroite, double minR, double maxR, double minA, double maxA) {
			
			//initialisation
			this.nbBat = nbBat;
			id = intDroite;
			
			bestLocal = new Data[nbBat];
			x = new Data[nbBat];
			v = new int[nbBat];
			f = new double[nbBat];
			r = new double[nbBat];
			initialR = new double[nbBat];
			a = new double[nbBat];
			
			for(int i=0; i<nbBat; i++) {
				r[i] = minR + (maxR - minR) * R.nextDouble();
				initialR[i] = r[i];
				a[i] = minA + (maxA - minA) * R.nextDouble();
				
				x[i] = D;
				bestLocal[i] = x[i];
				v[i] = 0;
				f[i] = 0.0;
			}
		}
		
		private void checkBest(Data D) throws InterruptedException {
			sem.acquire();
			double value = obj.value(D);
			if (value < bestValue){
				best = D;
				bestValue = value;
				System.out.println(bestValue);
			}
			sem.release();
		}

		@Override
		public void run() {
			long startime = System.currentTimeMillis();
			
			//cherche la premi�re meilleur valeur
			Data bestTmp = bestLocal[0];
			for (int i=1; i<nbBat; i++){
				if (obj.value(bestLocal[i]) < obj.value(bestTmp))
					bestTmp = bestLocal[i];
			}
			
			try {
				//actualise la solution si besoin
				checkBest(bestTmp);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//nouvelle solution pour chaque bat
			Data[] solution = new Data[nbBat];
			
			int t = 0;
			while (System.currentTimeMillis() - startime < maxTime && bestValue != 0) {
				
				for(int i=0; i<nbBat; i++) {
					//actualise f
					f[i] = id * R.nextDouble();
					//actualise v
					try {
						sem.acquire();
						int vTmp = (int) (v[i] + x[i].hammingDistanceTo(best) * f[i]);
						sem.release();
						if(vTmp < 1) vTmp = 1;
						if(vTmp > id) vTmp = id;
						v[i] = vTmp;
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					//actualise x
					solution[i] = x[i].randomSelectInNeighbour(v[i]);
				}
				
				for(int i=0; i<nbBat; i++) {
					//cherche new best
					if (R.nextDouble() > r[i]) {
						double A = a[0];
						for(int j=1; j<nbBat; j++) {
							A += a[j];
						}
						A /= nbBat;
						if(A < 1) A = 1;
						
						int newX = (int) (1 + A*(R.nextInt(3)));
						if(newX > id)
							newX = id;
						
						Data randomBest = bestLocal[R.nextInt(nbBat)];
						try {
							sem.acquire();
							x[i] = randomBest.randomSelectInNeighbour(newX);
							sem.release();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					Data newBest = x[i];
					//accepte la nouvelle solution
					if (obj.value(newBest) <= obj.value(bestLocal[i]) && R.nextDouble()*2 < a[i]){
						x[i] = solution[i];
						bestLocal[i] = newBest;
						//actualise a et r
						a[i] = a[i] * 0.9;
						r[i] = initialR[i] * (1 - Math.exp(-0.9*t));
					}
					
					try {
						//actualise la solution si besoin
						checkBest(solution[i]);
						checkBest(x[i]);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				t++;
			}
		}
	}
	
	public BatAlgo(Data startPoint,Objective obj,long maxTime, int nbBat, int nbThread)
   {
      try
      {
         String msg = "Impossible to create Bat object: ";
         if (maxTime <= 0) throw new Exception(msg + "the maximum execution time is 0 or even negative");
         this.maxTime = maxTime;
         if (startPoint == null) throw new Exception(msg + "the reference to the starting point is null");
         this.solution = startPoint;
         if (obj == null) throw new Exception(msg + "the reference to the objective is null");
         this.obj = obj;
         this.objValue = this.obj.value(this.solution);
         this.metaName = "Bat";
         
         R = new Random(System.currentTimeMillis());
         sem = new Semaphore(1, true);
         
         this.nbBats = nbBat;
         this.nbThread =nbThread;
      }
      catch (Exception e)
      {
         e.printStackTrace();
         System.exit(1);
      }
   }
	
	@Override
	public void optimize() {
		//prends la solution et valeur de d�part
		Data D = new Data(this.solution);
		best = D;
		bestValue = this.objValue;
		int n = D.numberOfBits();
		
		//initialise les threads de bats
		int nbBat = nbBats/nbThread;
		Thread[] lt = new Thread[nbThread];
		for(int i=0; i<nbThread; i++) {
			Bats b = new Bats(nbBat, obj.solutionSample(), n, 0.0, 1.0, 1.0, 2.0);
			lt[i] = new Thread(b);
			lt[i].start();
		}
		
		try {
			//attends la fin des threads
			for(int i=0; i<nbThread; i++) {
				lt[i].join();
			}
			
			//actualise la nouvelle solution et sa valeur
            this.objValue = bestValue;
            this.solution = best;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws InterruptedException
	   {
	      int ITMAX = 10000;  // number of iterations

	      // BitCounter
	      int n = 50;
	      Objective obj = new BitCounter(n);
	      Data D = obj.solutionSample();
	      BatAlgo b = new BatAlgo(D, obj, ITMAX, 100, 10);
	      System.out.println(b);
	      System.out.println("starting point : " + b.getSolution());
	      System.out.println("optimizing ...");
	      b.optimize();
	      System.out.println(b);
	      System.out.println("solution : " + b.getSolution());
	      System.out.println();
	      Thread.sleep(3000);
	      
	      // Fermat
	      int exp = 2;
	      int ndigits = 10;
	      obj = new Fermat(exp,ndigits);
	      D = obj.solutionSample();
	      b = new BatAlgo(D, obj, ITMAX, 100, 10);
	      System.out.println(b);
	      System.out.println("starting point : " + b.getSolution());
	      System.out.println("optimizing ...");
	      b.optimize();
	      System.out.println(b);
	      System.out.println("solution : " + b.getSolution());
	      Data x = new Data(b.solution,0,ndigits-1);
	      Data y = new Data(b.solution,ndigits,2*ndigits-1);
	      Data z = new Data(b.solution,2*ndigits,3*ndigits-1);
	      System.out.print("equivalent to the equation : " + x.posLongValue() + "^" + exp + " + " + y.posLongValue() + "^" + exp);
	      if (b.objValue == 0.0)
	         System.out.print(" == ");
	      else
	         System.out.print(" ?= ");
	      System.out.println(z.posLongValue() + "^" + exp);
	      System.out.println();
	      Thread.sleep(3000);
	      
	      // ColorPartition
	      n = 4;  int m = 15;
	      ColorPartition cp = new ColorPartition(n,m);
	      D = cp.solutionSample();
	      b = new BatAlgo(D, cp, ITMAX, 100, 10);
	      System.out.println(b);
	      System.out.println("starting point : " + b.getSolution());
	      System.out.println("optimizing ...");
	      b.optimize();
	      System.out.println(b);
	      System.out.println("solution : " + b.getSolution());
	      cp.value(b.solution);
	      System.out.println("corresponding to the matrix :\n" + cp.show());
	   }
}
